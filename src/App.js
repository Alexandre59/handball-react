import React, {Component} from 'react';
import './components/styles/css/Structure.css'
import './components/styles/css/Layout.css'
import Header from "./components/Header"
import OverlayMenu from "./components/OverlayMenu"
import Slider from "./components/sections/AwsSlider"
import HomeInfo from "./components/sections/HomeInfo"
import NextMatch from "./components/sections/NextMatch"
import LastResult from "./components/sections/LastResult"
import Players from "./components/sections/Players"

class App extends Component {
    constructor(props) {
        super(props)
        this.toggleMenu = this.toggleMenu.bind(this);
        this.state = { overlay: false, icon: "bars" }
    }
    toggleMenu() {
        this.setState({ overlay: !this.state.overlay})
    }

    render() {
        const {overlay, icon} = this.state
        return (
            <div>
                <Header overlay={overlay} icon={icon} onClick={this.toggleMenu} />
                {overlay && <OverlayMenu/>}
                <section className="main">
                    <Slider/>
                    <HomeInfo/>
                    <section className="flex_row full_width home__info">
                        <NextMatch/>
                        <LastResult/>
                    </section>
                    <Players/>
                </section>
            </div>
        );
    }
}

export default App
