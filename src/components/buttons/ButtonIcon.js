import React from "react"
import Icon from "../icons/Icon"

const ButtonIcon = ({ type }) => (
    <button type={type}>
        <Icon icon="info"/>
    </button>
)

export default ButtonIcon