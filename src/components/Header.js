import React from "react"
import PropTypes from "prop-types"
import logo from '../logo.png'
import './styles/css/Header.css'

const Header = ({ overlay, icon, onClick }) => (
    <header className="debug flex_row">
        <div className="header__logo debug">
            <img src={logo} alt=""/>
        </div>
        <div className="header__menu_button">
            <button onClick={onClick}>
                <i className={`fa fa-${icon}`} />
            </button>
        </div>
    </header>
)

Header.propTypes = {
    overlay: PropTypes.bool.isRequired,
    icon: PropTypes.oneOf([
        'bars',
        'times'
    ]).isRequired,
}

export default Header