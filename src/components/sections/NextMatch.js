import React from "react"
import '../styles/css/NextMatch.css'

const NextMatch = () => (
    <div className="home__next__match">
        <h2>Prochain match</h2>
        <div className="next__match__teams">
            <p>Forges-les-Eaux VS Gournay</p>
        </div>
        <div className="next__match__timer flex_row">
            <div className="timer__box flex_column">
                <span className="timer__numeric">20</span>
                <span className="timer__text">jours</span>
            </div>
            <div className="timer__box flex_column">
                <span className="timer__numeric">06</span>
                <span className="timer__text">heures</span>
            </div>
            <div className="timer__box flex_column">
                <span className="timer__numeric">30</span>
                <span className="timer__text">minutes</span>
            </div>
            <div className="timer__box flex_column">
                <span className="timer__numeric">30</span>
                <span className="timer__text">secondes</span>
            </div>
        </div>
    </div>
)

export default NextMatch