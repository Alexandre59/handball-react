import React from "react"
import ButtonIcon from "../buttons/ButtonIcon";

const HomeInfo = () => (
    <div className="home__info_button">
        <ButtonIcon type="button"/>
    </div>
)

export default HomeInfo