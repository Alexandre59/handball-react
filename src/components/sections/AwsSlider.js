import React, {Component} from 'react';
import AwesomeSlider from "react-awesome-slider"
import loader from "../../loading.png"
import image from "../../images/series/bojack-0.png"
import image2 from "../../images/series/bojack-1.png"
import image3 from "../../images/series/bojack-2.png"
import Style from  '../styles/scss/AwsSlider.scss';

const StartupScreen = () => (
    <div>
        <img src={loader} alt="Loader"/>
    </div>
)

class AwsSlider extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <AwesomeSlider startupscreen={StartupScreen} cssModule={Style}>
                <div data-src={image} />
                <div data-src={image2} />
                <div data-src={image3} />
            </AwesomeSlider>
        )
    }
}

export default AwsSlider

