import React from "react"
import '../styles/css/LastResult.css'

const LastResult = () => (
    <div className="last__result flex_column">
        <h2>Résultats dernier match</h2>
        <div className="last__result__teams flex_row">
            <div className="last__result_team_first">
            </div>
            <div className="last__result__info flex_column">
                <span className="last__result__info__date">September 28, 2018</span>
                <span className="last__result__info__score">0 - 2</span>
            </div>
            <div className="last__result_team_second">
            </div>
        </div>
        <div className="last__result__label">Rams VS Tigers</div>
    </div>
)

export default LastResult
