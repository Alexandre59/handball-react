import React, { Component } from "react"
import PropTypes from "prop-types"
import '../styles/css/Players.css'
import image from "../../images/player.jpg"

class Players extends Component {
    constructor(props) {
        super(props)
        this.state = {
            players: [],
            loading: false,
        };
    }

    componentDidMount() {
        this.setState({ loading: true })
        fetch('http://localhost:8000/api/players')
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error('Something went wrong ...');
                }
            })
            .then(data => this.setState({ players: data.players, loading: false }))
    }

    render() {
        const { players, loading } = this.state
        if (loading) {
            return <p>Loading...</p>
        }
        return (
            <ul>
                {players.map(player =>
                    <li key={player.id}>
                        <p>{player.firstname} {player.lastname}</p>
                    </li>
                )}
            </ul>
        )
    }
}

Players.propTypes = {
    players: PropTypes.arrayOf(
        PropTypes.shape({
            fistname: PropTypes.string,
            lastname: PropTypes.string
        })
    )
}

export default Players