import React from "react"
import './styles/scss/OverlayMenu.scss'

const OverlayMenu = () => (
    <section className="main__navigation__overlay flex_row">
        <div className="overlay__menu__container flex_column">
            <div className="navigation__container">
                <nav className="flex_column">
                    <a href="https://google.com">Les evenements</a>
                    <a href="https://google.com">Les resultats</a>
                    <a href="https://google.com">La galerie</a>
                    <a href="https://google.com">Les actualites</a>
                    <a href="https://google.com">Les entrainements</a>
                    <a href="https://google.com">Nos sponsors</a>
                </nav>
            </div>
        </div>
    </section>
)

export default OverlayMenu