import React from "react"

const Icon = ({ icon }) => (
    <i className={`fa fa-${icon}`}></i>
)

export default Icon